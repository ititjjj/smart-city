package com.sc.common.lock;/**
 * Created by wust on 2018/8/3.
 */

import java.util.concurrent.TimeUnit;

/**
 * Function:分布式锁接口
 * Reason:
 * Date:2018/8/3
 *
 * @author wusongti@lii.com.cn
 */
public interface DistributedLock {

    long LOCK_EXPIRE = 30000; // 锁过期时间

    long LOCK_TIMEOUT = 999999; //获取锁的超时时间

    /**
     * 尝试获取锁,不进行等待。
     * @param key
     * @return
     * @throws Exception
     */
    boolean tryLock(String key);

    /**
     * 阻塞等待获取锁
     * @param key
     * @throws Exception
     */
    boolean lock(String key);

    /**
     * 在规定时间内等待获取锁
     * @param key
     * @param time
     * @param unit
     * @return
     * @throws Exception
     */
    boolean lock(String key,long time, TimeUnit unit);

    /**
     * 释放锁
     * @param key
     * @throws Exception
     */
    boolean unLock(String key);

}
