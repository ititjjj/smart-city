package com.sc.common.dto;


import io.swagger.annotations.ApiModel;

@ApiModel(description = "分页对象")
public class PageDto {
	private static final long serialVersionUID = -5319064537975959840L;

	// 当前页数
	private int pageNum;

	// 每页显示记录
	private int pageSize;

	// 总记录
	private long total;

	// 当前记录起始索引
	private int currentResult;

	public int getPageNum() {
		return pageNum;
	}

	public void setPageNum(int pageNum) {
		this.pageNum = pageNum;
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public long getTotal() {
		return total;
	}

	public void setTotal(long total) {
		this.total = total;
	}

	public int getCurrentResult() {
		currentResult = (getPageNum() - 1) * getPageSize();
		if(currentResult < 0)
			currentResult = 0;
		return currentResult;
	}
	public void setCurrentResult(int currentResult) {
		this.currentResult = currentResult;
	}
}
