package com.sc.easyexcel.factory;/**
 * Created by wust on 2017/8/4.
 */

import com.sc.easyexcel.definition.ExcelDefinitionReader;

/**
 *
 * Function:抽象工厂
 * Reason:负责生产ExcelDefinitionReader
 * Date:2017/8/4
 * @author wust
 */
public interface DefinitionFactory {
    ExcelDefinitionReader createExcelDefinitionReader();
}
