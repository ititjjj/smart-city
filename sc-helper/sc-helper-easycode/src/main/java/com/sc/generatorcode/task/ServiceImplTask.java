package com.sc.generatorcode.task;

import com.sc.generatorcode.task.base.AbstractTask;
import com.sc.generatorcode.utils.ConfigUtil;
import com.sc.generatorcode.utils.FileUtil;
import com.sc.generatorcode.utils.FreemarkerConfigUtils;
import com.sc.generatorcode.utils.StringUtil;
import freemarker.template.TemplateException;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @author ：wust
 * @date：2019-12-26
 */
public class ServiceImplTask extends AbstractTask {

    public ServiceImplTask(String className) {
        super(className);
    }

    @Override
    public void run() throws IOException, TemplateException {
        String name = super.getName()[1];
        String entityPackageName = ConfigUtil.getConfiguration().getBasePackage().getEntityPackageName() + "." + name;


        // 生成Service填充数据
        Map<String, String> serviceData = new HashMap<>();
        serviceData.put("EntityPackageName", entityPackageName);
        serviceData.put("BasePackageName", ConfigUtil.getConfiguration().getBasePackage().getBase());
        serviceData.put("ServicePackageName", ConfigUtil.getConfiguration().getBasePackage().getService());
        serviceData.put("DaoPackageName", ConfigUtil.getConfiguration().getBasePackage().getDao());
        serviceData.put("Author", ConfigUtil.getConfiguration().getAuthor());
        serviceData.put("DateTime", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
        serviceData.put("ClassName", className);
        serviceData.put("EntityName", className);
        serviceData.put("EntityNameForCamelCase", StringUtil.firstToLowerCase(className));
        serviceData.put("Override", "\n    @Override");
        serviceData.put("InterfaceImport", "import " + ConfigUtil.getConfiguration().getBasePackage().getInterf().concat(".").concat(className).concat("Service;"));

        String filePath = FileUtil.getSourcePath() + StringUtil.package2Path(ConfigUtil.getConfiguration().getBasePackage().getService());
        String fileName = className + "ServiceImpl.java";
        FileUtil.generateFile(FreemarkerConfigUtils.TYPE_SERVICE, serviceData, filePath,fileName);
    }
}
