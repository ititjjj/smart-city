<?xml version="1.0" encoding="UTF-8"?>
<excel id="${xmlName}">
    <sheet label="${sheetLabel}">
        <list>
            <sql id="${sqlId}">
                <![CDATA[
                   ${sql}
                ]]>
            </sql>
            ${fields}
        </list>
    </sheet>
</excel>