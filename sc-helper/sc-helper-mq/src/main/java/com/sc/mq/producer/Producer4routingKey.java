/**
 * Created by wust on 2019-10-26 09:31:00
 * Copyright © 2019 wust. All rights reserved.
 */
package com.sc.mq.producer;

import com.alibaba.fastjson.JSONObject;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

/**
 * @author: wust
 * @date: Created in 2019-10-26 09:31:00
 * @description: DirectExchange专用生产者
 *
 */
@Component
public class Producer4routingKey {
    static Logger logger = LogManager.getLogger(Producer4routingKey.class);

    @Autowired
    private RabbitTemplate rabbitTemplate;

    @Autowired
    private Environment env;

    public void send(String exchangeName, String routingKey, JSONObject jsonObject){
        if(logger.isInfoEnabled()){
            logger.info("发送给指定exchangeName[{}],routingKey[{}]的生产者开始发送工作。。。",exchangeName,routingKey);
        }

        rabbitTemplate.setMessageConverter(new Jackson2JsonMessageConverter());
        rabbitTemplate.convertAndSend(exchangeName,routingKey,jsonObject);
        if(logger.isInfoEnabled()){
            logger.info("发送给指定exchangeName[{}],routingKey[{}]的生产者完成发送工作",exchangeName,routingKey);
        }
    }
}
