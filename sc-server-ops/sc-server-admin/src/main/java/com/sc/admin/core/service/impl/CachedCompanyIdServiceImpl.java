package com.sc.admin.core.service.impl;

import com.alibaba.fastjson.JSONArray;
import com.sc.common.enums.RedisKeyEnum;
import com.sc.common.service.InitializtionService;
import com.sc.common.util.PropertiesUtil;
import com.sc.common.util.cache.SpringRedisTools;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Service;
import java.util.Properties;
import java.util.Set;

/**
 * 缓存分公司ID
 */
@Order(0)
@Service
public class CachedCompanyIdServiceImpl implements InitializtionService {

    @Autowired
    private SpringRedisTools springRedisTools;

    @Override
    public void init() {
        Properties properties = PropertiesUtil.getDataSourceCompanyMapProperties();
        if(properties != null){
            Set<String> names = properties.stringPropertyNames();
            JSONArray jsonArray = new JSONArray(names.size());
            for (String name : names) {
                jsonArray.add(name);
            }


            String key = RedisKeyEnum.REDIS_KEY_COMPANY_ID_LIST.getStringValue();
            if(springRedisTools.hasKey(key)){
                springRedisTools.deleteByKey(key);
            }

            springRedisTools.addData(key,jsonArray.toJSONString());
        }
    }
}
