package com.sc.admin.core.service;


import com.sc.common.entity.admin.department.SysDepartment;
import com.sc.common.service.BaseService;


/**
 * Created by wust on 2019/6/3.
 */
public interface SysDepartmentService extends BaseService<SysDepartment> {
}
