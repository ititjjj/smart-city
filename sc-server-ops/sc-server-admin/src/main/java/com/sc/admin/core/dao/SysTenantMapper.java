package com.sc.admin.core.dao;

import com.sc.common.mapper.IBaseMapper;
import com.sc.admin.entity.tenant.SysTenant;

/**
 * @author: wust
 * @date: 2020-12-03 10:00:18
 * @description:
 */
public interface SysTenantMapper extends IBaseMapper<SysTenant>{
}