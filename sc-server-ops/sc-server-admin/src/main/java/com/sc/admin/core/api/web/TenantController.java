package com.sc.admin.core.api.web;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.sc.common.dto.PageDto;
import com.sc.admin.entity.tenant.SysTenant;
import com.sc.admin.entity.tenant.SysTenantList;
import com.sc.admin.entity.tenant.SysTenantSearch;
import com.sc.admin.core.service.SysTenantService;
import com.sc.common.annotations.WebApi;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.PathVariable;
import java.util.List;
import java.util.ArrayList;
import com.sc.common.dto.WebResponseDto;
import com.sc.common.context.DefaultBusinessContext;
import java.util.Date;
import cn.hutool.core.collection.CollectionUtil;
import org.springframework.beans.BeanUtils;
import org.springframework.core.env.Environment;

/**
 * @author: wust
 * @date: 2020-12-03 10:00:18
 * @description:
 */
@WebApi
@RequestMapping("/web/v1/TenantController")
@RestController
public class TenantController {
    @Autowired
    private SysTenantService sysTenantServiceImpl;

    @Autowired
    private Environment environment;

   @RequestMapping(value = "/listPage",method = RequestMethod.POST)
   public WebResponseDto listPage(@RequestBody SysTenantSearch search){
      WebResponseDto responseDto = new WebResponseDto();

      DefaultBusinessContext ctx = DefaultBusinessContext.getContext();

      PageDto pageDto = search.getPageDto();
      Page page = PageHelper.startPage(pageDto.getPageNum(),pageDto.getPageSize());
      List<SysTenant> list = sysTenantServiceImpl.select(search);
      if (CollectionUtil.isNotEmpty(list)) {
          List<SysTenantList> lists = new ArrayList<>(list.size());
          for (SysTenant entity : list) {
              SysTenantList entityList = new SysTenantList();
              BeanUtils.copyProperties(entity,entityList);
              lists.add(entityList);
          }
          responseDto.setLstDto(lists);
      }

      BeanUtils.copyProperties(page,pageDto);
      responseDto.setPage(pageDto);
      return responseDto;
   }



   @RequestMapping(value = "",method = RequestMethod.POST)
   public WebResponseDto create(@RequestBody  SysTenant entity){
       WebResponseDto responseDto = new WebResponseDto();
       DefaultBusinessContext ctx = DefaultBusinessContext.getContext();
       entity.setCreaterId(ctx.getAccountId());
       entity.setCreaterName(ctx.getAccountName());
       entity.setCreateTime(new Date());
       sysTenantServiceImpl.insert(entity);
       return responseDto;
   }


   @RequestMapping(value = "",method = RequestMethod.PUT)
   public WebResponseDto update(@RequestBody  SysTenant entity){
       WebResponseDto responseDto = new WebResponseDto();
       DefaultBusinessContext ctx = DefaultBusinessContext.getContext();
       entity.setModifyId(ctx.getAccountId());
       entity.setModifyName(ctx.getAccountName());
       entity.setModifyTime(new Date());
       sysTenantServiceImpl.updateByPrimaryKeySelective(entity);
       return responseDto;
   }


   @RequestMapping(value = "/{id}",method = RequestMethod.DELETE)
   public WebResponseDto delete(@PathVariable Long id){
       WebResponseDto responseDto = new WebResponseDto();
       sysTenantServiceImpl.deleteByPrimaryKey(id);
       return responseDto;
   }



    @RequestMapping(value = "/{id}",method = RequestMethod.GET)
    public WebResponseDto detail(@PathVariable Long id){
        WebResponseDto responseDto = new WebResponseDto();
        SysTenant entity = sysTenantServiceImpl.selectByPrimaryKey(id);
        if(entity != null){
            responseDto.setObj(entity);
        }else{
            responseDto.setFlag(WebResponseDto.INFO_WARNING);
            responseDto.setMessage("没有该记录！");
            return responseDto;
        }
        return responseDto;
    }
}
