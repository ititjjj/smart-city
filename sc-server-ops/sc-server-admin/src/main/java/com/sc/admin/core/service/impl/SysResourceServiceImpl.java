package com.sc.admin.core.service.impl;

import com.sc.admin.core.bo.ResourceBo;
import com.sc.admin.core.dao.SysDepartmentMapper;
import com.sc.admin.core.dao.SysOrganizationMapper;
import com.sc.admin.core.dao.SysResourceMapper;
import com.sc.admin.core.service.SysResourceService;
import com.sc.common.mapper.IBaseMapper;
import com.sc.common.entity.admin.resource.SysResource;
import com.sc.common.service.BaseServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("sysResourceServiceImpl")
public class SysResourceServiceImpl extends BaseServiceImpl<SysResource>  implements SysResourceService {

    @Autowired
    private ResourceBo resourceBo;

    @Autowired
    private SysOrganizationMapper sysOrganizationMapper;

    @Autowired
    private SysDepartmentMapper sysDepartmentMapper;

    @Autowired
    private SysResourceMapper sysResourceMapper;

    @Override
    protected IBaseMapper getBaseMapper() {
        return sysResourceMapper;
    }

    @Override
    public List<SysResource> findAllResources4superAdmin(String type, String lang) {
        return sysResourceMapper.findAllResources4superAdmin(type,lang);
    }

    @Override
    public List<SysResource> findAllAnonResources4superAdmin(String type,String lang) {
        return sysResourceMapper.findAllAnonResources4superAdmin(type,lang);
    }

    @Override
    public List<SysResource> findResources4admin(String permissionType, String lang, Long accountId) {
        return sysResourceMapper.findResources4admin(permissionType,lang,accountId);
    }

    @Override
    public List<SysResource> findAnonResources4admin(String permissionType, String lang, Long accountId) {
        return sysResourceMapper.findAnonResources4admin(permissionType,lang,accountId);
    }

    @Override
    public List<SysResource> findResourcesByUserId(String type,String lang,Long userId) {
        return sysResourceMapper.findResourcesByUserId(type,lang,userId);
    }

    @Override
    public List<SysResource> findAnonResourcesByUserId(String type,String lang,Long userId) {
        return sysResourceMapper.findAnonResourcesByUserId(type,lang,userId);
    }
}
