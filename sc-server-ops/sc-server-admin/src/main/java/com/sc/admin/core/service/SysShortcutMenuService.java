package com.sc.admin.core.service;

import com.sc.common.service.BaseService;
import com.sc.common.entity.admin.shortcutmenu.SysShortcutMenu;

/**
 * @author: wust
 * @date: 2020-02-16 11:25:19
 * @description:
 */
public interface SysShortcutMenuService extends BaseService<SysShortcutMenu>{
}
