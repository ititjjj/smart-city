/**
 * Created by wust on 2019-11-01 10:23:02
 * Copyright © 2019 wust. All rights reserved.
 */
package com.sc.admin.core.api.open;


import com.sc.admin.core.service.SysAccountService;
import com.sc.admin.core.service.SysUserService;
import com.sc.common.annotations.OpenApi;
import com.sc.common.annotations.OperationLog;
import com.sc.common.context.DefaultBusinessContext;
import com.sc.common.dto.WebResponseDto;
import com.sc.common.entity.admin.account.SysAccount;
import com.sc.common.enums.ApplicationEnum;
import com.sc.common.enums.OperationLogEnum;
import com.sc.common.enums.RedisKeyEnum;
import com.sc.common.util.MyStringUtils;
import com.sc.common.util.RC4;
import com.sc.common.util.cache.SpringRedisTools;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.Date;

/**
 * @author: wust
 * @date: Created in 2019-11-01 10:23:02
 * @description: 忘记密码找回开放接口
 *
 */
@Api(tags = {"开放接口~找回密码"})
@OpenApi
@RequestMapping("/api/open/v1/ForgetPasswordOpenApi")
@RestController
public class ForgetPasswordOpenApi {

    @Autowired
    private SysUserService sysUserServiceImpl;

    @Autowired
    private SysAccountService sysAccountServiceImpl;

    @Autowired
    private SpringRedisTools springRedisTools;

    @ApiOperation(value = "找回密码", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name="loginName",value="登陆账号",required=true,paramType="query"),
            @ApiImplicitParam(name="password",value="新密码",required=true,paramType="query"),
            @ApiImplicitParam(name="verificationCode",value="验证码",required=true,paramType="query")
    })
    @OperationLog(moduleName= OperationLogEnum.MODULE_COMMON,businessName="找回密码",operationType= OperationLogEnum.Update)
    @RequestMapping(value = "",method = RequestMethod.POST,produces ="application/json;charset=utf-8")
    public @ResponseBody
    WebResponseDto forgetPassword(@RequestParam String loginName,
                                  @RequestParam String password,
                                  @RequestParam String verificationCode) {
        WebResponseDto responseDto = new WebResponseDto();
        DefaultBusinessContext ctx = DefaultBusinessContext.getContext();

        if(MyStringUtils.isBlank(loginName)){
            responseDto.setFlag(WebResponseDto.INFO_WARNING);
            responseDto.setMessage("找回密码失败，请输入登陆账号");
            return responseDto;
        }

        if(MyStringUtils.isBlank(password)){
            responseDto.setFlag(WebResponseDto.INFO_WARNING);
            responseDto.setMessage("找回密码失败，请输入新密码");
            return responseDto;
        }

        if(MyStringUtils.isBlank(verificationCode)){
            responseDto.setFlag(WebResponseDto.INFO_WARNING);
            responseDto.setMessage("找回密码失败，请输入验证码");
            return responseDto;
        }

        String key = String.format(RedisKeyEnum.REDIS_KEY_STRING_VERIFICATION_CODE.getStringValue(),"3",loginName,verificationCode);
        if(!springRedisTools.hasKey(key)){
            responseDto.setFlag(WebResponseDto.INFO_WARNING);
            responseDto.setMessage("找回密码失败，无效的验证码");
            return responseDto;
        }
        springRedisTools.deleteByKey(key);

        SysAccount accountSearch = new SysAccount();
        accountSearch.setAccountCode(loginName);
        SysAccount account = sysAccountServiceImpl.selectOne(accountSearch);
        if(account != null){
            String passwordRC4 = RC4.encry_RC4_string(password, ApplicationEnum.LOGIN_RC4_KEY.getStringValue());
            account.setPassword(passwordRC4);
            account.setModifyId(ctx.getAccountId());
            account.setModifyName(ctx.getAccountName());
            account.setModifyTime(new Date());
            sysAccountServiceImpl.updateByPrimaryKeySelective(account);
        }
        return responseDto;
    }
}
