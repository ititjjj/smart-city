package com.sc.admin.core.dao;


import com.sc.common.mapper.IBaseMapper;
import com.sc.common.entity.admin.organization.SysOrganization;

/**
 * Created by wust on 2019/6/3.
 */
public interface SysOrganizationMapper  extends IBaseMapper<SysOrganization> {
}
