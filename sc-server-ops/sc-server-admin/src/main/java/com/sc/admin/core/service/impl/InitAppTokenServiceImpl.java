package com.sc.admin.core.service.impl;

import com.sc.admin.core.dao.SysAppTokenMapper;
import com.sc.common.entity.admin.apptoken.SysAppToken;
import com.sc.common.service.InitializtionService;
import com.sc.common.util.MyStringUtils;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import java.util.Date;

@Order(5)
@Service
public class InitAppTokenServiceImpl implements InitializtionService {
    @Autowired
    private Environment environment;

    @Autowired
    private SysAppTokenMapper sysAppTokenMapper;

    @Override
    public void init() {
        String appId1 = environment.getProperty("default-token.user-app.app-id");
        String secretKey1 = environment.getProperty("default-token.user-app.secret-key");
        Long signDuration1 = environment.getProperty("default-token.user-app.sign-duration",Long.class);
        String appIdExpireTime1 = environment.getProperty("default-token.user-app.app-id-expire-time");
        String description1 = environment.getProperty("default-token.user-app.description");

        String appId2 = environment.getProperty("default-token.customer-app.app-id");
        String secretKey2 = environment.getProperty("default-token.customer-app.secret-key");
        Long signDuration2 = environment.getProperty("default-token.customer-app.sign-duration",Long.class);
        String appIdExpireTime2 = environment.getProperty("default-token.customer-app.app-id-expire-time");
        String description2 = environment.getProperty("default-token.customer-app.description");

        String appId3 = environment.getProperty("default-token.user-web.app-id");
        String secretKey3 = environment.getProperty("default-token.user-web.secret-key");
        Long signDuration3 = environment.getProperty("default-token.user-web.sign-duration",Long.class);
        String appIdExpireTime3 = environment.getProperty("default-token.user-web.app-id-expire-time");
        String description3 = environment.getProperty("default-token.user-web.description");

        if(MyStringUtils.isNotBlank(MyStringUtils.null2String(appId1))){
            SysAppToken appTokenSearch = new SysAppToken();
            appTokenSearch.setAppId(appId1);
            SysAppToken appToken = sysAppTokenMapper.selectOne(appTokenSearch);
            if(appToken != null){
                appToken.setSignSecretKey(secretKey1);
                appToken.setSignDuration(signDuration1);
                appToken.setAppExpireTime(new DateTime(appIdExpireTime1).toDate());
                appToken.setSignSecretKey(secretKey1);
                appToken.setDescription(description1);
                appToken.setModifyTime(new Date());
                sysAppTokenMapper.updateByPrimaryKeySelective(appToken);
            }else{
                appToken = new SysAppToken();
                appToken.setAppId(appId1);
                appToken.setSignSecretKey(secretKey1);
                appToken.setSignDuration(signDuration1);
                appToken.setAppExpireTime(new DateTime(appIdExpireTime1).toDate());
                appToken.setDescription(description1);
                appToken.setCreateTime(new Date());
                appToken.setIsDeleted(0);
                sysAppTokenMapper.insert(appToken);
            }
        }

        if(MyStringUtils.isNotBlank(MyStringUtils.null2String(appId2))){
            SysAppToken appTokenSearch = new SysAppToken();
            appTokenSearch.setAppId(appId2);
            SysAppToken appToken = sysAppTokenMapper.selectOne(appTokenSearch);
            if(appToken != null){
                appToken.setSignSecretKey(secretKey2);
                appToken.setSignDuration(signDuration2);
                appToken.setAppExpireTime(new DateTime(appIdExpireTime2).toDate());
                appToken.setDescription(description2);
                appToken.setModifyTime(new Date());
                sysAppTokenMapper.updateByPrimaryKeySelective(appToken);
            }else{
                appToken = new SysAppToken();
                appToken.setAppId(appId2);
                appToken.setSignSecretKey(secretKey2);
                appToken.setSignDuration(signDuration2);
                appToken.setAppExpireTime(new DateTime(appIdExpireTime2).toDate());
                appToken.setDescription(description2);
                appToken.setCreateTime(new Date());
                appToken.setIsDeleted(0);
                sysAppTokenMapper.insert(appToken);
            }
        }

        if(MyStringUtils.isNotBlank(MyStringUtils.null2String(appId3))){
            SysAppToken appTokenSearch = new SysAppToken();
            appTokenSearch.setAppId(appId3);
            SysAppToken appToken = sysAppTokenMapper.selectOne(appTokenSearch);
            if(appToken != null){
                appToken.setSignSecretKey(secretKey3);
                appToken.setSignDuration(signDuration3);
                appToken.setAppExpireTime(new DateTime(appIdExpireTime3).toDate());
                appToken.setDescription(description3);
                appToken.setModifyTime(new Date());
                sysAppTokenMapper.updateByPrimaryKeySelective(appToken);
            }else{
                appToken = new SysAppToken();
                appToken.setAppId(appId3);
                appToken.setSignSecretKey(secretKey3);
                appToken.setSignDuration(signDuration3);
                appToken.setAppExpireTime(new DateTime(appIdExpireTime3).toDate());
                appToken.setDescription(description3);
                appToken.setCreateTime(new Date());
                appToken.setIsDeleted(0);
                sysAppTokenMapper.insert(appToken);
            }
        }
    }
}
