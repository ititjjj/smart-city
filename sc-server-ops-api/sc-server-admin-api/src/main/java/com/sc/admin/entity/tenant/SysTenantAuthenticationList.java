package com.sc.admin.entity.tenant;

import lombok.*;
import io.swagger.annotations.ApiModel;

/**
 * @author: wust
 * @date: 2020-12-03 10:21:44
 * @description:
 */
@ApiModel(description = "列表对象-SysTenantAuthenticationList")
@NoArgsConstructor
@ToString
@Data
public class SysTenantAuthenticationList extends SysTenantAuthentication {

}