package com.sc.admin.entity.post;

import lombok.*;
import io.swagger.annotations.ApiModel;

/**
 * @author: wust
 * @date: 2020-12-03 11:14:47
 * @description:
 */
  @ApiModel(description = "列表对象-SysPostList")
  @NoArgsConstructor
  @ToString
  @Data
public class SysPostList extends SysPost {

}