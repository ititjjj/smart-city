package com.sc.workorder.core.dao;

import com.sc.common.mapper.IBaseMapper;
import com.sc.workorder.entity.WoWorkOrderType;

/**
 * @author: wust
 * @date: 2020-04-01 10:13:20
 * @description:
 */
public interface WoWorkOrderTypeMapper extends IBaseMapper<WoWorkOrderType>{
}