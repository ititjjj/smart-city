package com.sc.workorder.core.service.impl;

import com.sc.common.context.DefaultBusinessContext;
import com.sc.workorder.entity.WoWorkOrderTimeline;
import com.sc.common.exception.BusinessException;
import com.sc.workorder.core.dao.WoWorkOrderMapper;
import com.sc.workorder.core.dao.WoWorkOrderTimelineMapper;
import com.sc.workorder.core.service.WoWorkOrderService;
import com.sc.workorder.entity.WoWorkOrder;
import com.sc.common.mapper.IBaseMapper;
import com.sc.common.service.BaseServiceImpl;
import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import com.sc.common.dto.WebResponseDto;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author: wust
 * @date: 2020-04-01 10:27:05
 * @description:
 */
@Service("woWorkOrderServiceImpl")
public class WoWorkOrderServiceImpl extends BaseServiceImpl<WoWorkOrder> implements WoWorkOrderService {
    @Autowired
    private WoWorkOrderMapper woWorkOrderMapper;

    @Autowired
    private WoWorkOrderTimelineMapper woWorkOrderTimelineMapper;

    @Override
    protected IBaseMapper getBaseMapper() {
        return woWorkOrderMapper;
    }


    @Transactional(rollbackFor=Exception.class)
    @Override
    public WebResponseDto create(Object obj) {
        WebResponseDto responseDto = new WebResponseDto();
        if(obj != null){
            List<WoWorkOrderTimeline> workOrderTimelines = new ArrayList<>(2);
            WoWorkOrder woWorkOrder = (WoWorkOrder)obj;
            if(woWorkOrder.getDirector() != null){
                woWorkOrder.setState("C103503");

                WoWorkOrderTimeline woWorkOrderTimeline = new WoWorkOrderTimeline();
                woWorkOrderTimeline.setWorkOrderNumber(woWorkOrder.getWorkOrderNumber());
                woWorkOrderTimeline.setType("C103501");
                woWorkOrderTimeline.setDescription("新建工单");
                woWorkOrderTimeline.setProjectId(woWorkOrder.getProjectId());
                woWorkOrderTimeline.setCompanyId(woWorkOrder.getCompanyId());
                woWorkOrderTimeline.setCreaterId(woWorkOrder.getCreaterId());
                woWorkOrderTimeline.setCreaterName(woWorkOrder.getCreaterName());
                woWorkOrderTimeline.setCreateTime(new Date());
                workOrderTimelines.add(woWorkOrderTimeline);


                WoWorkOrderTimeline woWorkOrderTimeline1 = new WoWorkOrderTimeline();
                woWorkOrderTimeline1.setWorkOrderNumber(woWorkOrder.getWorkOrderNumber());
                woWorkOrderTimeline1.setType("C103503");
                woWorkOrderTimeline1.setDescription("人工派单");
                woWorkOrderTimeline1.setProjectId(woWorkOrder.getProjectId());
                woWorkOrderTimeline1.setCompanyId(woWorkOrder.getCompanyId());
                woWorkOrderTimeline1.setCreaterId(woWorkOrder.getCreaterId());
                woWorkOrderTimeline1.setCreaterName(woWorkOrder.getCreaterName());
                woWorkOrderTimeline1.setCreateTime(new Date());
                workOrderTimelines.add(woWorkOrderTimeline1);
            }else{
                woWorkOrder.setState("C103501");
            }


            woWorkOrderMapper.insert(woWorkOrder);


            woWorkOrderTimelineMapper.insertList(workOrderTimelines);
        }
        return responseDto;
    }


    @Transactional(rollbackFor=Exception.class)
    @Override
    public WebResponseDto update(Object obj) {
        WebResponseDto responseDto = new WebResponseDto();
        return responseDto;
    }

    @Transactional(rollbackFor=Exception.class)
    @Override
    public WebResponseDto delete(Object obj) {
        WebResponseDto responseDto = new WebResponseDto();
        WoWorkOrder woWorkOrder = woWorkOrderMapper.selectByPrimaryKey(obj);
        if(woWorkOrder != null){
            woWorkOrderMapper.deleteByPrimaryKey(obj);

            WoWorkOrderTimeline woWorkOrderTimelineDelete = new WoWorkOrderTimeline();
            woWorkOrderTimelineDelete.setWorkOrderNumber(woWorkOrder.getWorkOrderNumber());
            woWorkOrderTimelineMapper.delete(woWorkOrderTimelineDelete);
        }
        return responseDto;
    }

    @Transactional(rollbackFor=Exception.class)
    @Override
    public void accept(Long id) {
        DefaultBusinessContext ctx = DefaultBusinessContext.getContext();
        WoWorkOrder woWorkOrder = woWorkOrderMapper.selectByPrimaryKey(id);
        if(woWorkOrder != null){
            if(!"C103501".equals(woWorkOrder.getState())){
                throw new BusinessException("该工单已经被其他人处理");
            }

            woWorkOrder.setDirector(ctx.getAccountId());
            woWorkOrder.setState("C103503"); // 进行中
            woWorkOrder.setModifyId(ctx.getAccountId());
            woWorkOrder.setModifyName(ctx.getAccountName());
            woWorkOrder.setModifyTime(new Date());
            woWorkOrderMapper.updateByPrimaryKeySelective(woWorkOrder);


            WoWorkOrderTimeline woWorkOrderTimeline = new WoWorkOrderTimeline();
            woWorkOrderTimeline.setType("C103503");
            woWorkOrderTimeline.setDescription("主动接单");
            woWorkOrderTimeline.setProjectId(woWorkOrder.getProjectId());
            woWorkOrderTimeline.setCompanyId(woWorkOrder.getCompanyId());
            woWorkOrderTimeline.setCreaterId(woWorkOrder.getCreaterId());
            woWorkOrderTimeline.setCreaterName(woWorkOrder.getCreaterName());
            woWorkOrderTimeline.setCreateTime(new Date());
            woWorkOrderTimelineMapper.insert(woWorkOrderTimeline);
        }
    }

    @Transactional(rollbackFor=Exception.class)
    @Override
    public void distribute(Long workOrderId, Long director) {
        DefaultBusinessContext ctx = DefaultBusinessContext.getContext();
        WoWorkOrder woWorkOrder = woWorkOrderMapper.selectByPrimaryKey(workOrderId);
        if(woWorkOrder != null){
            if(!"C103501".equals(woWorkOrder.getState())){
                throw new BusinessException("该工单已经被其他人处理");
            }

            woWorkOrder.setDirector(director);
            woWorkOrder.setState("C103503"); // 进行中
            woWorkOrder.setModifyId(ctx.getAccountId());
            woWorkOrder.setModifyName(ctx.getAccountName());
            woWorkOrder.setModifyTime(new Date());
            woWorkOrderMapper.updateByPrimaryKeySelective(woWorkOrder);


            WoWorkOrderTimeline woWorkOrderTimeline = new WoWorkOrderTimeline();
            woWorkOrderTimeline.setWorkOrderNumber(woWorkOrder.getWorkOrderNumber());
            woWorkOrderTimeline.setType("C103503");
            woWorkOrderTimeline.setDescription("人工派单");
            woWorkOrderTimeline.setProjectId(woWorkOrder.getProjectId());
            woWorkOrderTimeline.setCompanyId(woWorkOrder.getCompanyId());
            woWorkOrderTimeline.setCreaterId(woWorkOrder.getCreaterId());
            woWorkOrderTimeline.setCreaterName(woWorkOrder.getCreaterName());
            woWorkOrderTimeline.setCreateTime(new Date());
            woWorkOrderTimelineMapper.insert(woWorkOrderTimeline);
        }
    }

    @Transactional(rollbackFor=Exception.class)
    @Override
    public void finish(Long id) {
        DefaultBusinessContext ctx = DefaultBusinessContext.getContext();
        WoWorkOrder woWorkOrder = woWorkOrderMapper.selectByPrimaryKey(id);
        if(woWorkOrder != null){
            if(!"C103503".equals(woWorkOrder.getState())){
                throw new BusinessException("当前状态没有达到完成条件");
            }

            woWorkOrder.setState("C103503"); // 完成工单
            woWorkOrder.setModifyId(ctx.getAccountId());
            woWorkOrder.setModifyName(ctx.getAccountName());
            woWorkOrder.setModifyTime(new Date());
            woWorkOrderMapper.updateByPrimaryKeySelective(woWorkOrder);


            WoWorkOrderTimeline woWorkOrderTimeline = new WoWorkOrderTimeline();
            woWorkOrderTimeline.setType("C103503");
            woWorkOrderTimeline.setDescription("完成工单");
            woWorkOrderTimeline.setProjectId(woWorkOrder.getProjectId());
            woWorkOrderTimeline.setCompanyId(woWorkOrder.getCompanyId());
            woWorkOrderTimeline.setCreaterId(woWorkOrder.getCreaterId());
            woWorkOrderTimeline.setCreaterName(woWorkOrder.getCreaterName());
            woWorkOrderTimeline.setCreateTime(new Date());
            woWorkOrderTimelineMapper.insert(woWorkOrderTimeline);
        }
    }

    @Transactional(rollbackFor=Exception.class)
    @Override
    public void confirm(Long workOrderId, String description) {
        DefaultBusinessContext ctx = DefaultBusinessContext.getContext();
        WoWorkOrder woWorkOrder = woWorkOrderMapper.selectByPrimaryKey(workOrderId);
        if(woWorkOrder != null){
            if("C103507".equals(woWorkOrder.getState())){
                throw new BusinessException("该工单已经被其他人处理");
            }

            woWorkOrder.setState("C103507"); // 确认
            woWorkOrder.setModifyId(ctx.getAccountId());
            woWorkOrder.setModifyName(ctx.getAccountName());
            woWorkOrder.setModifyTime(new Date());
            woWorkOrderMapper.updateByPrimaryKeySelective(woWorkOrder);


            WoWorkOrderTimeline woWorkOrderTimeline = new WoWorkOrderTimeline();
            woWorkOrderTimeline.setType("C103507");
            woWorkOrderTimeline.setWorkOrderNumber(woWorkOrder.getWorkOrderNumber());
            woWorkOrderTimeline.setDescription(description);
            woWorkOrderTimeline.setProjectId(woWorkOrder.getProjectId());
            woWorkOrderTimeline.setCompanyId(woWorkOrder.getCompanyId());
            woWorkOrderTimeline.setCreaterId(woWorkOrder.getCreaterId());
            woWorkOrderTimeline.setCreaterName(woWorkOrder.getCreaterName());
            woWorkOrderTimeline.setCreateTime(new Date());
            woWorkOrderTimelineMapper.insert(woWorkOrderTimeline);
        }
    }
}
