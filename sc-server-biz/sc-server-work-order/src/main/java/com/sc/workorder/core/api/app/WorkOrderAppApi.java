/**
 * Created by wust on 2020-04-09 14:01:07
 * Copyright © 2020 wust. All rights reserved.
 */
package com.sc.workorder.core.api.app;

import com.sc.common.annotations.AppApi;
import com.sc.common.annotations.OperationLog;
import com.sc.common.context.DefaultBusinessContext;
import com.sc.common.dto.WebResponseDto;
import com.sc.common.enums.OperationLogEnum;
import com.sc.workorder.core.service.WoWorkOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @author: wust
 * @date: Created in 2020-04-09 14:01:07
 * @description: 工单接口
 *
 */
@AppApi
@RequestMapping("/api/app/v1/WorkOrderAppApi")
@RestController
public class WorkOrderAppApi {
    @Autowired
    private WoWorkOrderService woWorkOrderServiceImpl;

    @OperationLog(moduleName= OperationLogEnum.MODULE_WO_WORK_ORDER,businessName="接受订单",operationType= OperationLogEnum.Update)
    @RequestMapping(value = "/accept",method = RequestMethod.PUT,produces ="application/json;charset=utf-8")
    public WebResponseDto accept(@RequestParam Long id) {
        WebResponseDto responseDto = new WebResponseDto();
        DefaultBusinessContext ctx = DefaultBusinessContext.getContext();
        if(id == null){
            responseDto.setFlag(WebResponseDto.INFO_WARNING);
            responseDto.setMessage("参数不能为空");
            return responseDto;
        }

        woWorkOrderServiceImpl.accept(id);
        return responseDto;
    }


    @OperationLog(moduleName= OperationLogEnum.MODULE_WO_WORK_ORDER,businessName="完成订单",operationType= OperationLogEnum.Update)
    @RequestMapping(value = "/finish",method = RequestMethod.PUT,produces ="application/json;charset=utf-8")
    public WebResponseDto finish(@RequestParam Long id) {
        WebResponseDto responseDto = new WebResponseDto();
        DefaultBusinessContext ctx = DefaultBusinessContext.getContext();
        if(id == null){
            responseDto.setFlag(WebResponseDto.INFO_WARNING);
            responseDto.setMessage("参数不能为空");
            return responseDto;
        }

        woWorkOrderServiceImpl.finish(id);
        return responseDto;
    }
}
