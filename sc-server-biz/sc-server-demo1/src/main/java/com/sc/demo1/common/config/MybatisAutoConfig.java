package com.sc.demo1.common.config;


import com.sc.ds.config.ShardingsphereDataSourceConfigurationDefault;
import org.springframework.context.annotation.Configuration;


@Configuration
public class MybatisAutoConfig extends ShardingsphereDataSourceConfigurationDefault {
}
