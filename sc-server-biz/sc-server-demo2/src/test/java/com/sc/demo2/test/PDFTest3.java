package com.sc.demo2.test;

import com.sc.demo2.common.util.PDFUtil;
import java.util.HashMap;

/**
 * 增值税普通发票套打
 */
public class PDFTest3 {
    public static void main(String[] args) throws Exception {
        // PDF模板
        String templateFilePath = PDFUtil.getResourcePath4test() + " tax_tickets\\完税证明模板_wrapper.pdf";

        // 填充数据后的PDF
        String pdfFilePath = PDFUtil.getResourcePath4test() + " tax_tickets\\完税证明.pdf";


        // 表单文本域数据
        HashMap<String, String> formData = new HashMap<>();
        formData.put("text0", "1162xx02");
        formData.put("text1", "0370010xx0105");
        formData.put("text2", "1162xx02");
        formData.put("text3", "2020年08月06日");
        formData.put("text4", "南宁xx劳务服务有限公司");
        formData.put("text5", "91450103xxx55003R");
        formData.put("text6", "南宁市青秀区长湖路琅东七组五栋xx楼_0771-55266xx");
        formData.put("text7", "中国农业银行股份有限公司清秀万达分理处_10010104xx002358");
        formData.put("text8", "-*+--00119/3>/1>1175886jjfkjf+-?jeor+*jkjfkjkji899r4>djfoefjefeijfe+-jiji7878rg*j");
        formData.put("text9", "咨询服务费");
        formData.put("text10", "无");
        formData.put("text11", "无");
        formData.put("text12", "1");
        formData.put("text13", "14232.871287");
        formData.put("text14", "14232.87");
        formData.put("text15", "1%");
        formData.put("text16", "142.33");
        formData.put("text17", "￥14232.87");
        formData.put("text18", "￥142.33");
        formData.put("text19", "㊣壹万肆仟叁佰柒拾伍圆贰角整");
        formData.put("text19_1", "￥14375.20");
        formData.put("text20", "广西xx信息技术有限公司");
        formData.put("text21", "43387483748348378");
        formData.put("text22", "广西南宁市xxx 177279589xx");
        formData.put("text23", "2323233434343");
        formData.put("text24", "代开企业税号：232323232323  代开企业名称：广西xx信息技术有限公司");
        formData.put("text25", "小吴");
        formData.put("text26", "小吴");
        formData.put("text27", "小吴");

        // 生成PDF
        PDFUtil.createPDF(templateFilePath, pdfFilePath, formData, false);
    }
}
